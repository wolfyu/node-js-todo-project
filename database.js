var mysql       = require('mysql');

var envs = {
    local: {
        limit: 30,
        host: 'localhost',
        port: 3306,
        user: 'root',
        password: '',
        database: 'db_todo',
    }
}

var env     = envs.local;
var pool    = mysql.createPool({
    multipleStatements  : true,
    waitForConnections  : false,
    connectionLimit     : env.limit,
    host                : env.host,
    port                : env.port,
    user                : env.user,
    password            : env.password,
    database            : env.database
});

exports.getDB = (done) => {
    pool.getConnection((err, conn) => {
        if (err) {
            console.log("error get connection");
        } else {
            console.log("connection success");
        }
        done(err, conn);
    })
}