var jwt         = require('jsonwebtoken');
var error       = require('../rest/models/error');
var config      = require('../config');

exports.process = (token, req, done) => {
    if (token == null) {
        done(error.create(403, "No token provided"));
        return;
    }

    jwt.verify(token, config.secret, (err, decoded) => {
        if (err) {
            done(error.create(401, "Invalid Token"));
        } else if (decoded.id == null) {
            done(error.create(404, "User not found"));
        } else if (decoded.expired < (Date.now() / 1000)) {
            done(error.create(401, "Token Expired"));
        } else {
            req.decoded = decoded;
            console.log(decoded);
            done(null);
        }
    })   
}