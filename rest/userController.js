var express     = require('express');
var database    = require('../database');
var make        = require('../utils/response');
var str         = require('../constant')
var router      = express.Router();
var usermodel   = require('./models/User');
var jwt         = require('jsonwebtoken');
var config      = require('../config');

router.post('/user', (req, res) => {

    var user = {
        'username'  : req.body.username,
        'password'  : req.body.password,
        'email'     : req.body.email,
    }

    database.getDB((err, conn) => {
        if (err) {
            res.json(make.resp(500, str.errorGetDB, null));
        } else {
            usermodel.create(conn, user, (err, done) => {
                if (err) {
                    res.json(make.resp(err.code, err.message, done));
                } else {
                    res.json(make.resp(200, 'berhasil membuat user', done));
                }
            });
        }
    })
});

router.post('/user/login', (req, res) => {
    var user = {
        'username': req.body.username,
        'password': req.body.password
    }

    database.getDB((err, conn) => {
        if (err) {
            return res.json(make.resp(500, str.errorGetDB, null));
        }
        usermodel.login(conn, user, (err, done) => {
            if (err) {
                res.json(make.resp(err.code, err.message, done));
                return;
            }

            var expired = (Date.now() / 1000) + (1*60 * 60 * 24);
            console.log(expired);
            var payload = {
                'id'        : done.id,
                'expired'   : expired
            }

            var token = jwt.sign(payload, config.secret, {
                expiresIn: "" + expired
            })

            console.log(done);
            var result = {
                'token' : token,
                'email' : done.email,
                'id'    : done.id
            }

            res.json(make.resp(200, 'berhasil login', result));
        });
    })
})

router.patch('/user/changepassword', (req, res) => {

    var id_user = req.decoded.id;
    var newpass = req.body.new_password;
    var oldpass = req.body.old_password;

    database.getDB((err, conn) => {
        if (err) {
            return res.json(make.resp(500, str.errorGetDB, null));
        }
        usermodel.updatePassword(conn, id_user, newpass, oldpass, (err, done) => {
            if(err) {
                return res.json(make.resp(err.code, err.message, null));
            }

            res.json(make.resp(200, 'berhasil mengganti password', done));
        })

    });

});
module.exports = router

