var error       = require('./error');

exports.create  = (conn, user_id, task, done) => {

    if(!user_id) {
        return done(error.create(500, 'invalid decode cannot find user_id'));
    }
    if(!task.name) {
        return done(error.create(500, 'required payload "name"'));
    }
    if(!task.description) {
        return done(error.create(500, 'required payload "description"'));
    }
    if(!task.location) {
        return done(error.create(500, 'required payload "location"'));
    }
    if(!task.status) {
        return done(error.create(500, 'required payload "status"'));
    }
    if(!task.time_start) {
        return done(error.create(500, 'required payload "time_start"'));
    }
    if(!task.status) {
        return done(error.create(500, 'required payload "status"'));
    }
    if(["0","1"].indexOf(task.status) == -1) {
        return done(error.create(500, 'payload "status" only accept 0 or 1'));
    }
    if(!task.priority) {
        return done(error.create(500, 'required payload "priority"'));
    }
    if(["1","2","3","4","5"].indexOf(task.priority) == -1) {
        return done(error.create(500, 'required payload "priority" and priority only accept from 1 to 5'));
    }
    
    var sql = `INSERT INTO task (user_id, name, description, location, priority, status, time_start, date_created) 
    VALUES('${user_id}',
    '${task.name}',
    '${task.description}',
    '${task.location}',
    '${task.priority}',
    '${task.status}',
    '${task.time_start}', NOW())`;

    conn.query(sql, [], (err, rows) =>{
        if (err) {
            console.log(err);
            done(error.create(500, "internal server error"), null);
            return;
        }

        done(null, rows.insertId);
    });
}

exports.select = (conn, user_id, priority, status, done) => {
    
    var sql = `SELECT * FROM task WHERE task.user_id = '${user_id}' `

    if (status == "progress") {
        sql += `AND status = 0 `
    } else if(status == "done") {
        sql += `AND status = 1 `
    }
    
    sql +=`ORDER BY priority ${priority}`;

    conn.query(sql, (err, rows) => {
        console.log(sql);
        if (err) {
            console.log(err);
            done(error.create(500, err, rows), null);
            return;
        }

        done(null, rows);
    });
}

exports.update = (conn, user_id, task_id, task, done) => {

    if(!task_id) {
        return done(error.create(500, 'required parameter url at api/task/{id} for id task '));
    }
    if(!task.name) {
        return done(error.create(500, 'required payload "name"'));
    }
    if(!task.description) {
        return done(error.create(500, 'required payload "description"'));
    }
    if(!task.location) {
        return done(error.create(500, 'required payload "location"'));
    }
    if(!task.status) {
        return done(error.create(500, 'required payload "status"'));
    }
    if(["0","1"].indexOf(task.status) == -1) {
        return done(error.create(500, 'payload "status" only accept 0 or 1'));
    }
    if(!task.time_start) {
        return done(error.create(500, 'required payload "time_start"'));
    }
    if(!task.priority) {
        return done(error.create(500, 'required payload "priority"'));
    }
    if(["1","2","3","4","5"].indexOf(task.priority) == -1) {
        return done(error.create(500, 'priority only accept from 1 to 5'));
    }
    if(!task.status) {
        return done(error.create(500, 'required payload "status"'));
    }

    var sql = `UPDATE task SET name = '${task.name}',
    description = '${task.description}',
    location    = '${task.location}',
    status      = '${task.status}',
    time_start  = '${task.time_start}',
    priority    = '${task.priority}',
    date_updated = NOW() 
    WHERE task.id = '${task_id}' AND task.user_id = '${user_id}'`;
 
    conn.query(sql, [], (err, rows) =>{
        if (err) {
            console.log(err);
            done(error.create(500, err, rows), null);
            return;
        }

        if (rows.affectedRows == 0) {
            done(error.create(404, "task tidak di miliki oleh user", rows), null);
            return;
        }

        done(null, rows.affectedRows);
    });
}

exports.delete = (conn, user_id, task_id, done) => {

    if(!task_id) {
        return done(error.create(500, 'required payload "task_id"'));
    }
    var sql = `DELETE FROM task WHERE task.id = '${task_id}' AND task.user_id = '${user_id}'`;

    conn.query(sql, (err, rows) => {
        if (err) {
            console.log(err);
            done(error.create(500, err, rows), null);
            return;
        }
        if (rows.affectedRows == 0) {
            done(error.create(404, "task tidak di miliki oleh user", rows), null);
            return;
        }
        done(null, rows.affectedRows);
    });
}

exports.updatePriority = (conn, user_id, priority, id_task, done) => {
    if(!priority) {
        return done(error.create(500, 'required payload "priority"'));
    }
    if(["1","2","3","4","5"].indexOf(priority) == -1) {
        return done(error.create(500, 'priority only accept from 1 to 5'));
    }

    var sql = `UPDATE task SET priority = '${priority}' WHERE task.id = '${id_task}' AND task.user_id = '${user_id}'`;

    conn.query(sql, (err, rows) => {
        if (err) {
            console.log(err);
            done(error.create(500, err, rows), null);
            return;
        }

        if (rows.affectedRows == 0) {
            done(error.create(404, "task tidak di miliki oleh user", rows), null);
            return;
        }

        done(null, rows.affectedRows);
    });
}

exports.updateStatus = (conn, user_id, status, id_task, done) => {

    if(!status) {
        return done(error.create(500, 'required payload "status"'));
    }
    console.log(status);
    if(["0","1"].indexOf(status) == -1) {
        return done(error.create(500, 'payload "status" only accept 0 or 1'));
    }
    if(!id_task) {
        return done(error.create(500, 'required payload "id_task"'));
    }

    var sql = `UPDATE task SET status = '${status}' WHERE task.id = '${id_task}' AND task.user_id = '${user_id}'`;

    conn.query(sql, (err, rows) => {
        if (err) {
            console.log(err);
            done(error.create(500, err, rows), null);
            return;
        }

        if (rows.affectedRows == 0) {
            done(error.create(404, "task tidak di miliki oleh user", rows), null);
            return;
        }

        done(null, rows.affectedRows);
    });
}