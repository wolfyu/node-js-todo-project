var error   = require('./error');
var str     = require('../../constant');

var isUsernameIsTaken = (conn, username, done) => {
    var sql = `SELECT username FROM User WHERE username = '${username}'`;

    conn.query(sql, (err, rows) =>{
        if (err) {
            console.log(err);
            done(str.connError, true);
            return;
        }

        if (rows.length > 0) {
            done("Username already taken", true);
            return;
        }

        done(null, false);
    })
}

var isEmailTaken = (conn, email, done) => {
    var sql = `SELECT email FROM User WHERE email = '${email}'`;

    conn.query(sql, (err, rows) =>{
        if (err) {
            console.log(err);
            done(error.create(500, str.connError),true);
            return;
        }

        if (rows.length > 0) {
            done("Email already taken", true);
            return;
        }

        done(null, false);
    })
}

var getUser = (conn, id, done) => {
    var sql  = `SELECT * FROM user WHERE user.id = ${id}`;

    conn.query(sql, (err, rows) => {
        if (err) {
            done(null);
            return;
        }

        done(rows[0]);
    })
}

exports.create = (conn, user, done) => {

    if (!user.username) {
        done(error.create(500, "missing body 'username'"), null);
        return;
    }

    if (!user.password) {
        done(error.create(500, "missing body 'password'"), null);
        return;
    }

    if (!user.email) {
        done(error.create(500, "missing body 'email'"), null);
        return;
    }

    isUsernameIsTaken(conn, user.username, (err, result) => {
        if (err || result) {
            console.log(err || result);
            done(error.create(500,err), null);
        } else {
            isEmailTaken(conn, user.email, (err, result) => {
                if (err || result) {
                    console.log(err);
                    done(error.create(500,err), null);
                } else {
                    var sql = `INSERT INTO user (Username, Password, Email, date_created)
                    VALUE('${user.username}','${user.password}','${user.email}',NOW())`;

                    conn.query(sql, [], (err, rows) => {
                        if (err) {
                            console.log(err);
                            done(error.create(500,err), null);
                        } else {
                            done(null, rows.insertId);
                        }
                    })
                }
            })
        }
    })
}

exports.login = (conn, user, done) => {

    if (!user.username) {
        done(error.create(500, "missing body 'username'"), null);
        return;
    };
    
    if (!user.password) {
        done(error.create(500, "missing body 'password'"), null);
        return;
    }

    var sql = `SELECT * FROM user WHERE username = '${user.username}' AND password = '${user.password}'`;
    console.log(sql);

    var result = conn.query(sql, [], (err, rows) => {
        if (err) {
            console.log(err);
            done(error.create(500, str.connError), null);
            return;
        }

        if (rows.length == 0) {
            var err = error.create(403, "username atau password salah");
            done(err, null);
            return;
        }
        
        done(null, rows[0]);
    })
}

exports.updatePassword = (conn, id_user, newpassword, oldpassword, done) => {
    getUser(conn, id_user, (user) => {
        if(!user) {
            return done(error.create(404, 'User not found'));
        }
        if(!oldpassword) {
            return done(error.create(500, 'required payload "old_password"'));
        }
        if(!newpassword) {
            return done(error.create(500, 'required payload "new_password"'));
        }
        if(user.password != oldpassword) {
            return done(error.create(500, 'Password sebelumnya salah'));
        }

        var sql = `UPDATE user SET password = '${newpassword}' WHERE user.id = ${id_user}`;
        conn.query(sql, (err, result) => {
            if (err) {
                console.log(err);
                return done(error.create(500, 'Internal Server Error'));
            }

            if (result.affectedRows == 0) {
                console.log(err);
                return done(error.create(404, 'User not foung'));
            }

            done(null, result.affectedRows);
        })
    });
}
