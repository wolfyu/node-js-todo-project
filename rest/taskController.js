var express         = require('express');
var database        = require('./../database');
var make            = require('./../utils/response');
var str             = require('./../constant');
var router          = express.Router();
var taskmodel       = require('./models/task');

router.post('/task', (req, res, next) => {
    var user_id = req.decoded.id;
    var task = {
        'name': req.body.name,
        'description': req.body.description,
        'location': req.body.location,
        'priority': req.body.priority,
        'status': req.body.status,
        'time_start': req.body.time_start,
        'date': req.body.date,
    }
    console.log('user id:' + user_id);

    database.getDB((err, conn) => {
        if (err) {            
            res.json(make.resp(500, str.errorGetDB, null));
        }

        taskmodel.create(conn, user_id, task, (err, done) => {
            if (err) {
                return res.json(make.resp(err.code, err.message, null));
            }

            return res.json(make.resp(200, "Berhasil membuat task", done));
        })
    });
});

router.get('/task', (req, res, next) => {
    var user_id = req.decoded.id;
    var priority = req.query.priority; // optional
    var status = req.query.status; // optional

    if (['progress','done'].indexOf(status || "") == -1) {
        return res.json(make.resp(500, "param body 'status' accept only value string 'progress' or 'done' ", null));
    }

    if (['asc','desc'].indexOf(priority || "".toLowerCase()) == -1) {
        priority = 'ASC'
    }

    database.getDB((err, conn) => {
        if (err) {            
            res.json(make.resp(500, str.errorGetDB, null));
        }

        taskmodel.select(conn, user_id, priority, status, (err, done) => {
            if (err) {
                res.json(make.resp(500, err, null));
                return;
            } else {
                res.json(make.resp(200, "Berhasil mendapatkan task", done));
                return 
            }            
        })
    });
});

router.put('/task/:id',(req, res, next) => {
    
    var task_id = req.params.id;
    var user_id = req.decoded.id;
    var task = {
        'name': req.body.name,
        'description': req.body.description,
        'location': req.body.location,
        'status': req.body.status,
        'time_start': req.body.time_start,
        'date': req.body.date,
        'priority': req.body.priority
    }

    database.getDB((err, conn) => {
        if (err) {            
            res.json(make.resp(500, str.errorGetDB, null));
        }

        taskmodel.update(conn, user_id, task_id, task, (err, done) => {
            if (err) {
                return res.json(make.resp(err.code, err.message, null));
            }

            return res.json(make.resp(200, "Berhasil mengubah task", done));
        })
    });
})

router.patch('/task/:id/status',(req, res, next) => {

    var user_id = req.decoded.id;
    var status = req.body.status;
    var id_task = req.params.id;

    database.getDB((err, conn) => {
        if (err) {            
            res.json(make.resp(500, str.errorGetDB, null));
        }

        taskmodel.updateStatus(conn, user_id, status, id_task, (err, done) => {
            if (err) {
                return res.json(make.resp(err.code, err.message, null));
            }

            return res.json(make.resp(200, "Berhasil mengubah status task", done));
        })
    });
})

router.patch('/task/:id/priority',(req, res, next) => {

    var user_id = req.decoded.id;
    var priority = req.body.priority;
    var id_task = req.params.id;

    database.getDB((err, conn) => {
        if (err) {            
            res.json(make.resp(500, str.errorGetDB, null));
        }

        taskmodel.updatePriority(conn, user_id, priority, id_task, (err, done) => {
            if (err) {
                return res.json(make.resp(err.code, err.message, null));
            }

            return res.json(make.resp(200, "Berhasil mengubah status task", done));
        })
    });
})

router.delete('/task/:id', (req, res, next) => {
    var user_id = req.decoded.id;
    var task_id = req.params.id;

    database.getDB((err, conn) => {
        if (err) {            
            res.json(make.resp(500, str.errorGetDB, null));
        }

        taskmodel.delete(conn, user_id, task_id, (err, done) => {
            if (err) {
                return res.json(make.resp(err.code, err.message, null));
            }

            return res.json(make.resp(200, "Berhasil menghapus task", done));
        })
    });    
})

module.exports = router;