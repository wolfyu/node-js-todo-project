----# RestAPI ToDo App with Node JS #-----

Instalation:
	
	1. Clone repo
		git clone https://gitlab.com/wolfyu/node-js-todo-project.git

	2. Download module depedencies
		cd node-js-todo-project
		npm install

	3. Install Database
		install db_todo.sql to your own database management system

	4. Run
		node server.js

	5. (Opsional) test
		Install Postman 
		Import 'Todo Project.postman_collection.json' to Postman and test using that colletion

NOTE :
*The server should run on localhost:3000 plase make sure other application didnt use that port.
*You can see List Avaible API and paramter on Postman. 

///////////////////////////////////////

Framework :
- Express

Library using
- Body Parser
- dateformat
- jsonwebtoken
- mysql

Database :
- Mysql

//////////////////////////////////////

