exports.resp = (status, message, data) => {
    console.log("Response created: ["+status+"]:"+message);

    return {
        status: status,
        message: message,
        data: data
    }
}