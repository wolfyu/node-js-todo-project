var express         = require('express');
var bodyParser      = require('body-parser');
var auth            = require('./middleware/auth');
var app             = express();
var url             = require('url');
var make            = require('./utils/response');
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

var router          = express.Router();
router.use((req, res, next) => {
    var token   = req.headers["x-access-token"];
    var path    = url.parse(req.url).pathname;
    console.log('token  : ' + token);
    console.log('url    : ' + path);
    
    var whitelistUri = [];
    var method = req.method;
    if (method == "GET") {
        whitelistUri = [
        ]
    } else if(method == "POST") {
        whitelistUri = [
            '/user/login',
            '/register',
        ]
    } else if(method == "PUT") {
        whitelistUri = [
        ]
    } else if(method == "PATCH") {
        whitelistUri = [
        ]
    } else if(method == "DELETE") {
        whitelistUri = [
        ]
    }

    console.log(whitelistUri.indexOf(path));
    if (whitelistUri.indexOf(path) == -1) {
        console.log("required access token");
        auth.process(token, req, (err) => {
            if (err != null) {
                res.json(make.resp(err.code, err.message, null));
            } else {
                next();
            }
        })
    } else {
        console.log("without access token");
        next();
    }
});

app.use('/api',router);

var user = require('./rest/userController');
app.use('/api', user);
var task = require('./rest/taskController');
app.use('/api', task);

app.all('*', (req, res) => {
    res.json(make.resp(404, "API not found", null));
});

app.listen(process.env.PORT || 3000);